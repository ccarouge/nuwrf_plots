from typing import Dict
import xarray as xr
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from cycler import cycler # To choose the colours in the line plots
import os
import cartopy.crs as ccrs
import numpy as np

class MeanDiffMaps():

    def __init__(self, varcp, varunits):
        self.data={
            "varname":varcp,
            "units":varunits,
            "ref":None,
            "exp":[],
            "diff":[],
            "reldiff":[]
        }
        self.plot_opts={
            "mean":dict(),
            "diff":dict(),
            "reldiff":dict()
        }

    def get_plot_options(self,cmap_mean="coolwarm",
     cmap_diff="seismic", cmap_reldiff="seismic"):
        '''Compute the plot options for the panel plot
        for maps'''


        all_mean=list(self.data["exp"])
        all_mean.append(self.data["ref"])
        foo=xr.concat(all_mean,dim="new")
        self.plot_opts["mean"]["vmin"]=foo.chunk(-1).quantile(0.1).values
        self.plot_opts["mean"]["vmax"]=foo.chunk(-1).quantile(0.9).values
        self.plot_opts["mean"]["cmap"]=cmap_mean

        foo=xr.concat(self.data["diff"],dim="new")
        dmax=max(abs(foo.chunk(-1).quantile(0.1).values),abs(foo.chunk(-1).quantile(0.9).values))
        self.plot_opts["diff"]["vmin"]=-dmax
        self.plot_opts["diff"]["vmax"]=dmax
        self.plot_opts["diff"]["cmap"]=cmap_diff

        foo=xr.concat(self.data["reldiff"],dim="new")
        reldmax=max(abs(foo.chunk(-1).quantile(0.1).values),abs(foo.chunk(-1).quantile(0.9).values))
        if reldmax == float('inf'):
            reldmax = 1.00
        self.plot_opts["reldiff"]["vmin"]=-reldmax
        self.plot_opts["reldiff"]["vmax"]=reldmax
        self.plot_opts["reldiff"]["cmap"]=cmap_reldiff
        
        return self

    @staticmethod
    def plot_top_single_panel(ax, data, plot_opts,ext=None):
        '''Plot one of the panels in the panel plot'''

        if "XLONG" in data.coords:
            lon_nm = "XLONG"
            lat_nm = "XLAT"
            data.plot.pcolormesh(lon_nm,lat_nm,ax=ax,infer_intervals=True,transform=ccrs.PlateCarree(),**plot_opts)
            if not ext:
                ext = [100,np.round(data.coords[lon_nm].max().values,0),np.round(data.coords[lat_nm].min().values,0),np.round(data.coords[lat_nm].max().values,0)]
            ax.set_extent(ext)
            ax.coastlines() 
        else:
            lon_nm = "lon"
            lat_nm = "lat"
            if "soil_layers" in data.coords:
                data.isel(soil_layers=0).plot(ax=ax,**plot_opts)
            else:
                data.plot(ax=ax,**plot_opts)

             
        ax.set_title(data.plot_name)
        ax.set_xlabel("")
        ax.set_ylabel("")
    
    @staticmethod
    def plot_bottom_single_panel(ax,data,plot_opts,ext=None):
        '''Plot one of the panels in the last row of the panel plot'''

        if "XLONG" in data.coords:
            lon_nm = "XLONG"
            lat_nm = "XLAT"
            data.plot.pcolormesh(lon_nm,lat_nm,ax=ax,infer_intervals=True,transform=ccrs.PlateCarree(),**plot_opts)
            if not ext:
                ext = [100,np.round(data.coords[lon_nm].max().values,0),np.round(data.coords[lat_nm].min().values,0),np.round(data.coords[lat_nm].max().values,0)]
            ax.set_extent(ext)
            ax.coastlines() 
        else:
            lon_nm = "lon"
            lat_nm = "lat"
            if "soil_layers" in data.coords:
                data.isel(soil_layers=0).plot(ax=ax,**plot_opts)
            else:
                data.plot(ax=ax,**plot_opts)
                
        ax.set_title(data.plot_name)
        ax.set_xlabel("")
        ax.set_ylabel("")


    # Plotting
    def plot_mean_diff_mappanel(self,timestamp,pfig,cart_proj,figpref="mean_diff_panel",ext=None):
        # Need a panel plot with 4 rows and # experiments in columns
        nrows=4
        ncols=len(self.data["exp"])

        fig,axs = plt.subplots(nrows,ncols,figsize=(4*ncols,4*nrows),sharey=True,sharex=True,squeeze=False,subplot_kw={'projection': cart_proj})#,constrained_layout=True)
        fig.suptitle("Monthly mean "+self.data["varname"]+" ["+self.data["units"]+"]",fontsize="xx-large",y=0.95)

        # Draw ref:
        self.plot_top_single_panel(axs[0,0],self.data["ref"],self.plot_opts["mean"],ext)
        for i,exp in enumerate(self.data["exp"]):
            self.plot_top_single_panel(axs[1,i],exp,self.plot_opts["mean"],ext)
        for i,diff in enumerate(self.data["diff"]):
            self.plot_top_single_panel(axs[2,i],diff,self.plot_opts["diff"],ext)
        for i,reldiff in enumerate(self.data["reldiff"]):
            self.plot_bottom_single_panel(axs[3,i],reldiff,self.plot_opts["reldiff"],ext)

        fig.text(0.5, 0.08, 'Longitude', ha='center',fontsize="x-large")
        fig.text(0.04, 0.5, 'Latitude', va='center', rotation='vertical',fontsize="x-large")

        # Delete useless axes on first row
        for i in range(1,ncols):
            fig.delaxes(axs[0,i])
        fname=f"{figpref}_{self.data['varname']}_{timestamp}.png"
        pathpanel=pfig/"panel_maps"
        if not pathpanel.is_dir():
            os.makedirs(pathpanel)

        plt.savefig(pathpanel/fname)
        plt.close()


class MeanDiffTs():

    def __init__(self,varcp):
        self.data={
            "varname":varcp,
            "mean":None,
            "diff":None,
            "reldiff":None
        }
        self.plot_opts={
            "mean":dict(),
            "diff":dict(),
            "reldiff":dict()
        }

    def get_plot_options(self):
        '''Compute the plot options for the panel plot
        for maps'''
        
        return self

    def plot_ts_whole_domain(self,pfig,figpref="ts_wholedomain_panel"):
        '''Produce a panel line plots with the timeseries average over the whole
        domain. Top row is control and experiments, second row is the differences,
        third row is the relative differences'''

        nrows=3
        ncols=1

        fig,axs = plt.subplots(nrows,ncols,figsize=(10,15),sharex=True,squeeze=False)#,constrained_layout=True)
        fig.suptitle("Whole domain mean "+self.data["varname"],fontsize="xx-large",y=0.95)

        # Draw ref:
        colorlist=matplotlib.cm.get_cmap("Set1").colors
        custom_cycler = cycler(color=colorlist)
        axs[0,0].set_prop_cycle(custom_cycler)
        cycler2=cycler(color=colorlist[1:])
        axs[1,0].set_prop_cycle(cycler2)
        axs[2,0].set_prop_cycle(cycler2)
        self.data["mean"].plot(ax=axs[0,0],hue="experience")
        self.data["diff"].plot(ax=axs[1,0],hue="experience")
        self.data["reldiff"].plot(ax=axs[2,0],hue="experience")
        fname=f"{figpref}_{self.data['varname']}.png"
        pathts=pfig/"timeseries"
        if not pathts.is_dir():
            os.makedirs(pathts)
        plt.savefig(pathts/fname)
        plt.close()
        

