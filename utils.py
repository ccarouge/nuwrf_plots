# Utility function for plotting comparison plots for NUWRF experiments
import xarray as xr
import pandas as pd
import numpy as np
import wrf
import glob
import netCDF4 as nc
import xesmf as xe

def get_varname(vars, ref_data):
    '''Append '-inst' or '_tavg' suffix to the names in vars.
    vars: List of variable names as strings
    ref_data: Dataset from LIS output
    
    varscp: List of variable names as strings with the correct suffix
            for the experiment.'''

    varscp=[]
    for var in vars:
        foo=var+"_inst"
        if (foo not in ref_data.data_vars.keys()):
            foo=var+"_tavg"
        varscp.append(foo)
    
    return varscp

def read_data(info, dates_range):
    '''Read the correct input file and add the file path and name of experiment 
    to the attributes. Select one level along the soil_layers dimension and
    mask the data using the landmask variable in the file. 
    info: Dictionary 
        info["path"]: Path object, location of the LIS output files
        info["name"]: Name of the LIS experiment to use for plot titles
    dates_range: pandas Date range
    ilevel: index of soil_layers to select
    landmaskvar: name of the Landmask variable
    
    data: Dataset, add information from info as global attributes
    validcount: DataArray, number of valid land points in the landmask variable'''

    stamplist=[foo.strftime("%Y%m-%Y%m") for foo in dates_range]
    fref=[info["path"]/f"LIS.CABLE.{timestamp}.nc" for timestamp in stamplist]
    data = xr.open_mfdataset(fref)
    data = data.assign_attrs(name=info["name"],path=info["path"])

    return data

def apply_mask(data, landmaskvar):
    '''Mask the data using the landmask variable in the file. 
    And count the number of valid land points.
    
    Assumption: the land-sea mask is invariant in time'''

    data = data.where(np.isnan(data[landmaskvar].isel(time=0).values)==False)
    validcount = data[landmaskvar].isel(time=0).count().load()

    return data, validcount

def prepare_data(info, dates_range, ilevel=0):
    '''Read the correct input file and add the file path and name of experiment 
    to the attributes. Select one level along the soil_layers dimension and
    mask the data using the landmask variable in the file. 
    info: Dictionary 
        info["path"]: Path object, location of the LIS output files
        info["name"]: Name of the LIS experiment to use for plot titles
    dates_range: pandas Date range
    ilevel: index of soil_layers to select
    landmaskvar: name of the Landmask variable
    
    data: Dataset, add information from info as global attributes
    validcount: DataArray, number of valid land points in the landmask variable'''

    data = read_data(info, dates_range)

    if 'soil_layers' in data.dims: # ALH perhaps not the best approach but works for now
        data = data.isel(soil_layers=ilevel)
    else:
        data = data.isel(dict(SoilMoist_profiles=ilevel,SoilTemp_profiles=ilevel,SmLiqFrac_profiles=ilevel,SmFrozFrac_profiles=ilevel,RelSMC_profiles=ilevel))
    landmaskvar=get_varname(["Landmask"],data)[0]
    data, validcount = apply_mask(data, landmaskvar)

    return data, validcount

def get_variable_2D(dataset,varcp,level=0):
    '''Get the variable from the dataset and subset to the correct level if 3D variable
    dataset: Dataset from LIS output
    varcp: String, name of the variable to read
    level: Integer, the level to select the data on
    
    var: DataArray of dimensions (time, lat, lon)'''

    var=dataset[varcp]
    if "soil_layers" in var.dims:
        var=var.isel(soil_layers=level)
    
    return var

def prepare_wrf(info, dates_range, varlist, ilevel=0, mask=False):
    '''Read the correct input file and add the file path and name of experiment 
    to the attributes. Select one level along the soil_layers dimension and
    mask the data using the landmask variable in the file. 
    info: Dictionary 
        info["path"]: Path object, location of the LIS output files
        info["name"]: Name of the LIS experiment to use for plot titles
    dates_range: pandas Date range
    ilevel: index of vertical layers to select
    mask: flag to indicate whether to mask the ocean points
    
    data: Dataset, add information from info as global attributes'''

    data = read_wrf(info, dates_range, varlist)

    if mask == True:
        print('Masking ocean points')
        data = data.where(data['LANDMASK'] > 0)

    return data

def read_wrf(info, dates_range,varlist):
    '''Read the correct input file and add the file path and name of experiment 
    to the attributes. 
    info: Dictionary 
        info["path"]: Path object, location of the LIS output files
        info["name"]: Name of the LIS experiment to use for plot titles
    dates_range: pandas Date range
    varlist: list of the variables to retrieve
    
    data: Dataset, add information from info as global attributes
    validcount: DataArray, number of valid land points in the landmask variable'''

    # Get a list of all the files and select the relevant files
    filelist = sorted(glob.glob('%s/%s_%s_*' %(info["path"],info["prefix"],info["domain"])))
    stamplist=[foo.strftime("%Y-%m-%d") for foo in dates_range]

    selec_wrffiles = sel_files(filelist,info["domain"],stamplist)

    wrffiles = []
    for ff in range(len(selec_wrffiles)):
        wrffiles.append(nc.Dataset(selec_wrffiles[ff]))
    
    wrftime = wrf.getvar(wrffiles,"times",timeidx=None,method='cat') # Times
    wrftime = wrftime.astype('datetime64[ns]')
    central_longitude = nc.Dataset(selec_wrffiles[0]).getncattr('CEN_LON')
    
    for vv in range(len(varlist)): # Can handle multiple variables as required
        if vv == 0:
            data = wrf.getvar(wrffiles,varlist[vv],timeidx=None,method='cat')
        else:
            tmpvar = wrf.getvar(wrffiles,varlist[vv],timeidx=None,method='cat')
            data = xr.merge([data,tmpvar]) # adding subsequent variables to the xr.Dataset
            del tmpvar
        
    if "RAINC" in varlist:
        data["precip"] = data["RAINC"] + data["RAINNC"]
    if "RAINCVMEAN" in varlist:
        data["precip"] = (data["RAINCVMEAN"] + data["RAINNCVMEAN"]) * 86400
    if "T2MIN" in varlist:
        data["tmin"] = data["T2MIN"]# - 273.15
    if "T2MAX" in varlist:
        data["tmax"] = data["T2MAX"]# - 273.15

    if info["prefix"] in ["wrfout"]:
        wrfmask  = wrf.getvar(wrffiles,"LANDMASK",timeidx=None,method='cat') # Landmask
        lmask = wrfmask.isel(Time=0)
        data = xr.merge([data,lmask],compat='override')
    if info["prefix"] in ["wrfxtrm"]:
        geofile = glob.glob('%s/geo_em.%s.nc' %(info["invariant"],info["domain"]))[0]
        geodata = xr.open_dataset(geofile)
        lmask = geodata["LANDMASK"].isel(Time=0)
        data = xr.merge([data,lmask])

    data = data.assign_attrs(name=info["name"],path=info["path"],cen_lon=central_longitude)

    # Need to handle the time dimension/coord name
    data = data.rename_dims({'Time':'time'})
    time = wrftime
    data = data.assign_coords(time=("time", time))

    # deal with duplicate entries
    _, index = np.unique(data['time'], return_index=True)

    return data.isel(time=index)

def sel_files(filelist,pattern,dates_range):
    '''Gets all the dates of the WRF output files and selects the
    files that correspond to the date range of interest'''
    filenamedates=np.array([fname.split(pattern)[-1][1:11] for fname in filelist])
    sel_files=[n for i,n in enumerate(filelist) if  ((filenamedates[i]<= dates_range[-1]) & (filenamedates[i]>= dates_range[0]))]
    return sel_files

def prepare_obs(info, dates_range):
    '''Read the correct input file and add the file path and name of experiment 
    to the attributes.  
    info: Dictionary 
        info["path"]: Path object, location of the dataset
        info["name"]: Name of the dataset to use for plot titles
        info["varnm"]: List of dataset variables

    dates_range: pandas Date range
    
    data: Dataset, add information from info as global attributes'''

    # Lookup table to select the correct data type for each variable
    vtype={
        "precip":"total",
        "tmax":"mean",
        "tmin":"mean"
    }

    datalist=[]
    for vname in info["varnm"]:
        filelist=sorted(info["path"].glob(f"{vname}/{vtype[vname]}/**/01day/*{vname}*.nc"))
        data = xr.open_mfdataset(filelist).sel(
            time=slice(dates_range[0].strftime("%Y"),
                       dates_range[-1].strftime("%Y"))
        )
        datalist.append(data)
    data = xr.merge(datalist,compat='override')
        
    data = data.assign_attrs(name=info["name"],path=info["path"])

    return data[info["varnm"]] # In this way we don't return the lat_bnds and lon_bnds

def regrid_data(info,indata,target_grid,method='xesmf'):
    '''Regrids the input data to the target grid.
    info: Dictionary
        info["path"]: Path object, location of the dataset
        info["name"]: Name of the dataset to use for plot titles
        info["varnm"]: List of dataset variables


    indata: the input data that you want to regrid
    target_grid: an xarray dataset defining the target grid

    data: Dataset'''

    if method == 'xesmf':
        regridder = xe.Regridder(indata, target_grid, 'bilinear')
        data = regridder(indata)
    else:
        print('this is yet to be developed - no regridding performed - data returned on native grid :(')
        data = indata

    for vind,vname in enumerate(info["varnm"]):
        data[vname].attrs["units"] = info["varunits"][vind]
        
    data = xr.merge([data,target_grid])

    data = data.assign_attrs(name=info["name"],path=info["path"])

    return data


