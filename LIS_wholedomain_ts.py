# 
# Description
#------------
# Calculate the mean over the whole domain and then plot a panel plot of timeseries such as:
# - row 1: timeseries of all experiments, control in black
# - row 2: timeseries of exp-ref for each experiment
# - row 3: timeseries of (exp-ref)/ref for each experiment

import xarray as xr
from plotlib import mean_diff_plot as MDP
import utils
import statlib
from pathlib import Path
import pandas as pd
import os
import dask
import tempfile
import numpy as np

def plot_var_ts(varcp,ref_mean,exp_data,diff_data,reldiff_data,pfig):
        # Define the MeanDiffPlot object to hold the data for the plot
        plot_data = MDP.MeanDiffTs(varcp)

        # Get the variable arrays and load in memory
        # mean over domain
        ref_var=ref_mean[varcp]

        mean_exp=[]
        diff_exp=[]
        reldiff_exp=[]
        experience=[ref_mean.name]
        for exp,diff,reldiff in zip(exp_data,diff_data,reldiff_data):
            exp_var=exp[varcp]
            diff_var=diff[varcp]
            reldiff_var=reldiff[varcp]
 
            # Add to the list of experiment data
            mean_exp.append(exp_var)
            diff_exp.append(diff_var)
            reldiff_exp.append(reldiff_var)
            experience.append(exp.name)

        # Concatenate mean, diff and reldiff into a single DataArray with a new experience dimension.
        plot_data.data["varname"]=varcp
        foo=[ref_var]
        foo.extend(mean_exp)
        plot_data.data["mean"]=xr.concat(foo,dim="experience")
        plot_data.data["diff"]=xr.concat(diff_exp,dim="experience")
        plot_data.data["reldiff"]=xr.concat(reldiff_exp,dim="experience")

        plot_data.data["mean"]=plot_data.data["mean"].assign_coords({"experience":experience})
        plot_data.data["diff"]=plot_data.data["diff"].assign_coords({"experience":experience[1:]})
        plot_data.data["reldiff"]=plot_data.data["reldiff"].assign_coords({"experience":experience[1:]})

        plot_data.plot_ts_whole_domain(pfig)

def LIS_wholedomain_ts(ref_info, exp_info, dates_range, varscp, landmaskvar,pfig):
    '''Calculate the mean over the whole domain and then plot a panel plot of timeseries such as:
         - row 1: timeseries of the control experiment, ref
         - row 2: timeseries of each experiment, exp
         - row 3: timeseries of exp-ref for each experiment
         - row 4: timeseries of (exp-ref)/ref for each experiment
       We assume the 
         
       ref_data: DataArray of the control experiment:
                    - 1D
                    - must contain a time dimension named "time"
                    - must include a "plot_name" attribute containing the name of the control
                 experiment to use for titles in plots
       exp_data: List of DataArrays of the experiments. 
                 Each DataArray: 
                    - is 1D.
                    - must contain a time dimension named "time"
                    - must include a "plot_name" attribute containing the name of the 
                 experiment to use for titles in plots'''

    # Read data:
    # List of files to read in for the control:
    stamplist=[foo.strftime("%Y%m-%Y%m") for foo in dates_range]
    fref=[ref_info["path"]/f"LIS.CABLE.{timestamp}.nc" for timestamp in stamplist]
    ref_data = xr.open_mfdataset(fref)
    ref_data = ref_data.assign_attrs(name=ref_info["name"],path=ref_info["path"])
    ref_data = ref_data.isel(soil_layers=0)
    ref_data = ref_data.where(np.isnan(ref_data[landmaskvar].isel(time=0).values)==False)
    ref_validcount = ref_data[landmaskvar].isel(time=0).count().load()

    exp_data=[]
    diff_data=[]
    reldiff_data=[]
    for exp in exp_info:
        stamplist=[foo.strftime("%Y%m-%Y%m") for foo in dates_range]
        fexp=[exp["path"]/f"LIS.CABLE.{timestamp}.nc" for timestamp in stamplist]
        foo= xr.open_mfdataset(fexp)
        foo = foo.assign_attrs(name=exp["name"],path=exp["path"])
        foo = foo.isel(soil_layers=0)
        foo = foo.where(np.isnan(foo[landmaskvar].isel(time=0).values)==False)
        foo_validcount = foo[landmaskvar].isel(time=0).count().load()

        exp_mean = statlib.get_domain_mean(foo,foo_validcount,name=foo.name)
        exp_mean = exp_mean.assign_attrs(name=exp["name"],path=exp["path"])
        exp_data.append(exp_mean)
        foo1=ref_data-foo
        diff_mean=statlib.get_domain_mean(foo1,foo_validcount,name=foo.name)
        diff_mean = diff_mean.assign_attrs(name=exp["name"],path=exp["path"])

        diff_data.append(diff_mean)
        reldiff = foo1/ref_data
        reldiff= statlib.get_domain_mean(reldiff,foo_validcount,name=foo.name)
        diff_mean = diff_mean.assign_attrs(name=exp["name"],path=exp["path"])
        reldiff_data.append(reldiff)

    ref_mean = statlib.get_domain_mean(ref_data,ref_validcount,name=ref_data.name)
    ref_mean = ref_mean.assign_attrs(name=ref_info["name"],path=ref_info["path"])

    # Loop over variables:
    for varcp in varscp:
        print("Plotting variable: ",varcp)
        plot_var_ts(varcp,ref_mean,exp_data,diff_data,reldiff_data,pfig)

if __name__ == "__main__":

    client = dask.distributed.Client(
        n_workers=8, threads_per_worker=1,
        memory_limit='4gb', local_directory=tempfile.mkdtemp())

    # information for the reference experiment. 
    # Dictionary with a path and a name to use for titles in plots
    ref_info={
        "name":"hires",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/hires/LIS_output")
    }

    # Path to LIS experiments. 
    # Each experiment is a dictionary of a path and a name to use for titles in plots.
    exp_info=[
        {"name":"update",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/hires_up/LIS_output")
        }
    ]

    # Start date:
    start_date="1994-06"
    end_date = "1999-05"
    # Time period to analyse
    dates_range=pd.date_range(start=start_date,end=end_date,freq="MS")

    # Variables to compare
    # Code will search for var_inst and var_tavg
    vars=["SoilTemp","Evap","SoilMoist","RelSMC","LAI",
          "LWdown_f","SWdown_f","Psurf_f","Qair_f","Tair_f",
          "Rainf_f","Wind_f","CanopInt","ESoil","TVeg","ECanop",
          "SoilWet","SmFrozFrac","SmLiqFrac","SWE","Albedo",
          "AvgSurfT","Qsb","Qs","Rainf","Qg","Qh","Qle","Lwnet","Swnet"]

    timestamp=dates_range[0].strftime("%Y%m-%Y%m")
    ref_data=xr.open_dataset(ref_info["path"]/f"LIS.CABLE.{timestamp}.nc")
    varscp=utils.get_varname(vars, ref_data)
    landmaskvar=utils.get_varname(["Landmask"],ref_data)[0]

    # Create plot directory if it doesn't exist
    pfig=Path()/"figures"
    if not pfig.is_dir():
        os.makedirs(pfig)

    LIS_wholedomain_ts(ref_info, exp_info, dates_range, varscp,landmaskvar,pfig)
