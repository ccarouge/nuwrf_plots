import xarray as xr

def get_timemean(var, name=""):
    '''Compute mean in time of the variable, remove name of the array and add the attribute
    "plot_name" for plotting purposes.
    
    var: DataArray with dimension "time"
    name: string, name to use to design the experiment
    
    var_mean: DataArray, mean of var along time with an additional plot_name attribute'''

    var_mean=var.mean(dim="time")
    var_mean=var_mean.rename("")
    var_mean=var_mean.assign_attrs({"plot_name":name})

    return var_mean

def get_domain_mean(var, count, name=""):
    '''Compute mean in space of the variable, remove name of the array and add the attribute
    "plot_name" for plotting purposes.
    
    var: DataArray with dimensions "lat" and "lon"
    name: string, name to use to design the experiment
    
    var_mean: DataArray, mean of var along time with an additional plot_name attribute'''

    # Get the names of all the dimensions but time
    mean_dims=[dim for dim in var.dims if dim != "time"]
    var_mean=var.sum(dim=mean_dims)
    var_mean = var_mean/count
    #var_mean=var_mean.rename("")
    var_mean=var_mean.assign_attrs({"plot_name":name})

    return var_mean


# %%
def get_diff(var, ref, meandim='time',name=""):
    '''Compute mean in time of var-ref, remove name of the array and add the attribute
    "plot_name" for plotting purposes.
    
    var, ref: DataArrays with the same dimensions. One needs to be "time"
    meandim: 'time'/'space', To choose to average along 'time' or 'space' dimensions 
    name: string, name to use to design the experiment
    
    var_diff: DataArray, mean of var-ref along "time" with an additional plot_name attribute'''
    
    var_diff=(var-ref)
    if meandim=='space':
        meandim=[dim for dim in var_diff.dims if dim !="time"]
    var_diff=var_diff.mean(dim=meandim)
    var_diff=var_diff.rename("")
    var_diff=var_diff.assign_attrs({"plot_name":"diff: "+name})

    return var_diff


# %%
def get_reldiff(var, ref, meandim='time', name=""):
    '''Compute mean in time of (var-ref)/ref, remove name of the array and add the attribute
    "plot_name" for plotting purposes.
    
    var, ref: DataArrays with the same dimensions. One needs to be "time". 
    meandim: 'time'/'space', To choose to average along 'time' or 'space' dimensions 
    name: string, name to use to design the experiment
    
    var_diff: DataArray, mean of (var-ref)/ref along "time" with an additional plot_name attribute'''

    var_reldiff=((var-ref)/ref)
    if meandim=='space':
        meandim=[dim for dim in var_reldiff.dims if dim !="time"]
    var_reldiff=var_reldiff.mean(dim=meandim)
    var_reldiff=var_reldiff.rename("")
    var_reldiff=var_reldiff.assign_attrs({"plot_name":"rel. diff:"+name})

    return var_reldiff
