# 
# Description
#-------------
# compare different LIS experiments to a control LIS experiment.
# It works on monthly output files for LIS (default output of the 
# automatic post-processing).
# It will produce a panel plot of 4 rows:
# - row 1: mean in time of the control experiment, ref
# - row 2: mean in time of each experiment, exp
# - row 3: mean in time of exp-ref for each experiment
# - row 4: mean in time of (exp-ref)/ref for each experiment
#
# The output will be saved in a figures/ subdirectory created
# in the current working directory.

from pathlib import Path
import pandas as pd
import matplotlib
import os
import utils
import LIS_maps
import tempfile
import dask
import cartopy.crs as ccrs
import numpy as np


if __name__ == "__main__":

    client = dask.distributed.Client(
        n_workers=8, threads_per_worker=1,
        memory_limit='4gb', local_directory=tempfile.mkdtemp())

    # Path to the reference experiment and the name to use for titles in plots
    ref_info={
        "name":"lowres_oldCABLE",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE/LIS_output")
    }

    # Path to LIS experiments. 
    # Each experiment is a dictionary of a path and a name to use for titles in plots.
    exp_info=[
        {"name":"lowres_r7264",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264/LIS_output")
        }
    ]

    # Start date:
    start_date="1999-01"
    end_date = "1999-01"

    # Variables to compare
    # Code will search for var_inst and var_tavg
    vars=["SoilTemp","Evap","SoilMoist","RelSMC","LAI",
        "LWdown_f","SWdown_f","Psurf_f","Qair_f","Tair_f",
        "Rainf_f","Wind_f","CanopInt","ESoil","TVeg","ECanop",
        "SoilWet","SmFrozFrac","SmLiqFrac","SWE","Albedo",
        "AvgSurfT","Qsb","Qs","Rainf","Qg","Qh","Qle","Lwnet","Swnet"]

    # Level to plot for soil data. Give the index: 0 to 5.
    ilevel=0

    # Create plot directory if it doesn't exist
    pfig=Path()/"figures"
    if not pfig.is_dir():
        os.makedirs(pfig)


    # Time period to analyse
    dates_range=pd.date_range(start=start_date,end=end_date,freq="MS")

    # Prepare data: read the data, choose a level for soil_layers and mask with the landmask for each experiment
    ref_data,ref_validcount = utils.prepare_data(ref_info, dates_range, ilevel)
    exp_data=[]
    for experience in exp_info:
        exp, exp_validcount=utils.prepare_data(experience, dates_range, ilevel)
        exp_data.append(exp)

    # Get the correct variable names with "_inst" or "_tavg" suffix
    varscp=utils.get_varname(vars,ref_data)

    # Define the cartopy mapping object
    cart_proj = ccrs.LambertConformal(central_longitude=ref_data.attrs["STANDARD_LON"],cutoff=np.round(ref_data.coords["lat"].min().values,0))
    
    # Plot maps:
    print("Start panels")
    LIS_maps.monthly_panels(ref_data, exp_data, dates_range, varscp, pfig, cart_proj)

