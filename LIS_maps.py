import utils
from plotlib import mean_diff_plot as MDP
import statlib

def monthly_panels(ref_data, exp_data, dates_range, varscp, pfig, cart_proj, ext=None):
    # Loop through each month to produce a panel plot for each monthly mean and differences
    for timestamp in dates_range:

        seltime=timestamp.strftime("%Y-%m")
        filetime=timestamp.strftime("%Y%m")

        print("Plots for date: ", seltime)

        # Read data for this month:
        ref=ref_data.sel(time=seltime)

        explist=[]
        for experience in exp_data:
            exp=experience.sel(time=seltime)
            explist.append(exp)

        # Get the monthly mean of the first variable
        for varcp in varscp:

            print("Plot for var: ", varcp)
            plot_data=MDP.MeanDiffMaps(varcp,ref_data[varcp].attrs["units"])

            ref_var=ref[varcp]

            #Mean in time and save for plot on first line of panel plot
            plot_data.data["ref"]=statlib.get_timemean(ref_var, ref_data.name)
            
            for experience in explist:
                exp_var=experience[varcp]
                exp_mean=statlib.get_timemean(exp_var, experience.name)
                exp_diff=statlib.get_diff(exp_var, ref_var, 'time', experience.name)
                exp_reldiff=statlib.get_reldiff(exp_var, ref_var, 'time',experience.name)
            
                plot_data.data["exp"].append(exp_mean)
                plot_data.data["diff"].append(exp_diff)
                plot_data.data["reldiff"].append(exp_reldiff)
                
            # Plot mean/diff panel plot for this variable and month
            # Get plot options:
            plot_data=plot_data.get_plot_options()
            plot_data.plot_mean_diff_mappanel(filetime,pfig,cart_proj,ext=ext)
