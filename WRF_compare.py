# 
# Description
#-------------
# compare different WRF experiments to a control WRF experiment.
# It works on monthly output files for WRF (default output of the 
# automatic post-processing).
# It will produce a panel plot of 4 rows:
# - row 1: mean in time of the control experiment, ref
# - row 2: mean in time of each experiment, exp
# - row 3: mean in time of exp-ref for each experiment
# - row 4: mean in time of (exp-ref)/ref for each experiment
#
# The output will be saved in a figures/ subdirectory created
# in the current working directory.

from pathlib import Path
import pandas as pd
import matplotlib
import os
import utils
import LIS_maps
import tempfile
import dask
import wrf
import cartopy.crs as ccrs
import numpy as np


if __name__ == "__main__":

    client = dask.distributed.Client(
        n_workers=8, threads_per_worker=1,
        memory_limit='4gb', local_directory=tempfile.mkdtemp())

    # User input arguments for the reference experiment. 
    # name: label to use for plot titles
    # path: path to the outputs
    # invariant: path to the geo_em.d??.nc file containing invariant variables
    # prefix: WRF output file type: options are wrfout or wrfxtrm
    # domain: the domain to compare
    ref_info={
        "name":"lowres_oldCABLE",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE/"),
        "prefix":"wrfout",
        "domain":"d01"
    }

    # As per the reference experiment but for the other WRF experiments.
    exp_info=[
        {"name":"lowres_r7264",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264/"),
        "prefix":"wrfout",
        "domain":"d01"
        }
    ]

    # Start date:
    start_date="1999-06"
    end_date = "1999-09"
    # NOTE : date indexing by the month, not daily

    # Variables to compare
    # Starting with 2D variables
    vars=["T2","Q2"]

    # Level to plot for atmospheric levels. Not used at the moment.
    ilevel=0

    # Create plot directory if it doesn't exist
    pfig=Path()/"figures"
    if not pfig.is_dir():
        os.makedirs(pfig)

    # Time period to analyse
    dates_range=pd.date_range(start=start_date,end=end_date,freq="MS")

    # Prepare data: read the data, oceans can be masked if mask=True
    ref_data = utils.prepare_wrf(ref_info, dates_range,vars, ilevel, mask=True)
    exp_data=[]
    for experience in exp_info:
        exp = utils.prepare_wrf(experience, dates_range, vars, ilevel, mask=True)
        exp_data.append(exp)

    # Define the cartopy mapping object
    cart_proj = ccrs.LambertConformal(central_longitude=ref_data.attrs["cen_lon"],cutoff=np.round(ref_data.coords["XLAT"].min().values,0))

    # Plot maps:
    print("Start panels")
    LIS_maps.monthly_panels(ref_data, exp_data, dates_range, vars, pfig, cart_proj)

