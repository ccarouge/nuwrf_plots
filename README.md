This package is to produce comparison plots between NUWRF outputs. It is intended as first checks of the raw outputs.

**LIS_compare.py**: compare different LIS experiments to a control LIS experiment. It works on monthly output files for LIS (default output of the automatic post-processing). See description in the file for more details on the outputs.


