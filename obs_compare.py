# 
# Description
#-------------
# compare different WRF/LIS experiments to observations.
# It works on monthly output files for WRF (default output of the 
# automatic post-processing).
# It will produce a panel plot of 4 rows:
# - row 1: mean in time of the control experiment, ref
# - row 2: mean in time of each experiment, exp
# - row 3: mean in time of exp-ref for each experiment
# - row 4: mean in time of (exp-ref)/ref for each experiment
#
# The output will be saved in a figures/ subdirectory created
# in the current working directory.

from pathlib import Path
import pandas as pd
import matplotlib
import os
import utils
import LIS_maps
import tempfile
import dask
import wrf
import cartopy.crs as ccrs
import numpy as np
import xarray as xr
import datetime as dt

print("MSG: If running on gadi using conda/analysis3-test, xe.Regridder requires longitude and latitude")
print("MSG: If running on vdi using conda/analysis3-20.10, xe.Regridder requires lon and lat")

if __name__ == "__main__":

#    client = dask.distributed.Client(
#        n_workers=8, threads_per_worker=1,
#        memory_limit='4gb', local_directory=tempfile.mkdtemp())

    # User input arguments for the observational data
    # name: label to use for plot titles
    # path: path to the outputs
    # varnm: variable names
    # varunits: units of the variables listed in varnm
    ref_info={
        "name":"AGCD",
        "path":Path("/g/data/zv2/agcd/v1/"),
        "varnm":["precip","tmax","tmin"],
        "varunits":["mm","degC","degC"]
        }

    # User input arguments for the model experiments. 
    # name: label to use for plot titles
    # path: path to the outputs
    # invariant: path to the geo_em.d??.nc file containing invariant variables
    # prefix: WRF output file type: options are wrfout or wrfxtrm
    # domain: the domain to compare
    exp_info=[
        {
        "name":"lowres_oldCABLE",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE/"),
        "prefix":"wrfxtrm",
        "domain":"d01"
        },
        {
        "name":"lowres_oldCABLE_phys2",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE_phys2/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_oldCABLE_phys2/"),
        "prefix":"wrfxtrm",
        "domain":"d01"
        },
        {"name":"lowres_r7264",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264/"),
        "prefix":"wrfxtrm",
        "domain":"d01"
        },
        {"name":"lowres_r7264_phys2",
        "path":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264_phys2/WRF_output"),
        "invariant":Path("/g/data/w35/ccc561/LIS-WRF/LISWRF_configs/lowres_r7264_phys2/"),
        "prefix":"wrfxtrm",
        "domain":"d01"
        }
    ]

    # Start date:
    start_date="2000-03"
    end_date = "2000-03"
    start_day="01"
    end_day="31"

    sdate = dt.datetime.strftime(pd.to_datetime('%s-%s' %(start_date,start_day)),'%Y-%m-%d')
    edate = dt.datetime.strftime(pd.to_datetime('%s-%s' %(end_date,end_day)),'%Y-%m-%d')

    # Variables to compare
    # Starting with 2D variables
    vars=["T2MIN","T2MAX","RAINCVMEAN","RAINNCVMEAN"]

    # Level to plot for atmospheric levels. Not used at the moment.
    ilevel=0

    # Create plot directory if it doesn't exist
    pfig=Path()/"figures"
    if not pfig.is_dir():
        os.makedirs(pfig)

    # Time period to analyse
    dates_range=pd.date_range(start=start_date,end=end_date,freq="MS")

    # Prepare data: read the data, oceans can be masked if mask=True
    ref_data = utils.prepare_obs(ref_info, dates_range)
    exp_data=[]
    for experience in exp_info:
        exp = utils.prepare_wrf(experience, dates_range, vars, ilevel, mask=True)
        expstind = [i for i in range(len(exp['time'].values)) if dt.datetime.strftime(pd.to_datetime(exp['time'].values[i]),'%Y-%m-%d') in [sdate]][0]
        expfiind = [i for i in range(len(exp['time'].values)) if dt.datetime.strftime(pd.to_datetime(exp['time'].values[i]),'%Y-%m-%d') in [edate]][0]
        exp_data.append(exp.isel(time=slice(expstind,expfiind+1)))

    # Take landmask and projection info from first experiment
    foo = exp_data[0]
    ds_out = foo['LANDMASK'].copy(deep=False)
    #ds_out = ds_out.rename({'XLONG': 'lon', 'XLAT': 'lat'})
    ds_out = ds_out.rename({'XLONG': 'longitude', 'XLAT': 'latitude'})
    cart_proj = ccrs.LambertConformal(central_longitude=foo.attrs["cen_lon"],cutoff=np.round(foo.coords["XLAT"].min().values,0))
    
    # Regrid the observational data to the model grid, mask the ocean points
    ref_data_regrid = utils.regrid_data(ref_info,ref_data,ds_out,method='xesmf')
    #ref_data_regrid = ref_data_regrid.rename({'lon': 'XLONG', 'lat': 'XLAT'})
    ref_data_regrid = ref_data_regrid.rename({'longitude': 'XLONG', 'latitude': 'XLAT'})
    ref_data_regrid = ref_data_regrid.where(ref_data_regrid['LANDMASK'] > 0)

    # Select the correct time slice
    refstind = [i for i in range(len(ref_data_regrid['time'].values)) if dt.datetime.strftime(pd.to_datetime(ref_data_regrid['time'].values[i]) - dt.timedelta(hours=9),'%Y-%m-%d') in [sdate]][0]
    reffiind = [i for i in range(len(ref_data_regrid['time'].values)) if dt.datetime.strftime(pd.to_datetime(ref_data_regrid['time'].values[i]) - dt.timedelta(hours=9),'%Y-%m-%d') in [edate]][0]
    ref_data_timeslice = ref_data_regrid.isel(time=slice(refstind,reffiind+1))
    ref_data_timeslice['time'] = exp_data[0]['time']

    # set the extent
    ext = [113.,155.,-43.,-9]

    # Plot maps:
    print("Start panels")
    LIS_maps.monthly_panels(ref_data_timeslice, exp_data, dates_range, ref_info["varnm"], pfig, cart_proj, ext)



